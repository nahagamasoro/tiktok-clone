import { Link } from '@mui/material';
import { Icon } from '@iconify/react';

export default function SidebarLink({
  active,
  icon,
  label,
  href,
}: {
  active?: boolean;
  icon: string;
  label: string;
  href: string;
}) {
  return (
    <Link
      href={href}
      sx={{
        textDecoration: 'none',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        fontSize: '18px',
        padding: '10px',
        fontWeight: 'bold',
        color: active ? 'rgba(254, 44, 85, 1)' : '#000000',
        ':hover': {
          backgroundColor: 'rgba(22, 24, 35, .06)',
        },
      }}
    >
      <>
        <Icon icon={icon} style={{ fontSize: 28, marginRight: 10 }} />

        {label}
      </>
    </Link>
  );
}
