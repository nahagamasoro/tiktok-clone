import { Button, Divider, Stack, Typography } from "@mui/material";
import SidebarLink from "./SidebarLink";
import SuggestedAccounts from "./SuggestedAccounts";

export default function Sidebar() {
  return (
    <Stack width="30%" spacing={2}>
      <SidebarLink
        active
        icon="ant-design:home-filled"
        label="Pour toi"
        href="#"
      />

      <SidebarLink icon="majesticons:users-line" label="Abonnements" href="#" />

      <SidebarLink icon="ri:live-line" label="LIVE" href="#" />

      <Divider />

      <Typography variant="body1" sx={{ color: "rgba(22, 24, 35, .5)" }}>
        Connecte-toi pour suivre des créateurs, aimer des vidéos et voir les
        commentaires.
      </Typography>

      <Button
        disableElevation
        sx={{
          backgroundColor: "#FFFFFF",
          border: "1px solid rgba(254, 44, 85, 1)",
          borderRadius: "2px",
          textTransform: "none",
          fontWeight: "bold",
          fontSize: "18px",
          color: "rgba(254, 44, 85, 1)",
        }}
        variant="outlined"
      >
        Connexion
      </Button>

      <Divider />

      <SuggestedAccounts />
    </Stack>
  );
}
