export const PrimaryButtonStyle = {
    backgroundColor: "rgba(254, 44, 85, 1) ",
    border: "none",
    borderRadius: "4px",
    color: "rgba(255, 255, 255, 1)",
    textTransform: "none",
    fontWeight: "bold",
    ":hover": {
        backgroundColor: "rgba(254, 44, 85, 1) ",
        border: "none",
        borderRadius: "4px",
        color: "rgba(255, 255, 255, 1)",
        textTransform: "none",
        fontWeight: "bold",
    },
}
